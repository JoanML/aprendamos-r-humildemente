# Qué es esto

Esto es un intento de cursillo de R de confinamiento. Si me preguntan mis empleadores, diré que estoy haciendo
"dissemination", "training" o algo así. Totalmente para lo que me pagan.

# Requisitos

Hay que instalar:

* R: por favor la 3.6 o superior por si acaso
* RStudio (no, no es lo mismo)
* El entorno tidyverse (en R, `install.packages("tidyverse")`)

# Temario

Para darle seriedad, hay temario:

1. Cosas muy muy básicas de programación
1. Leer y manipular datos
1. Descriptiva
1. Estimación puntual, intervalos de confianza y contraste de hipótesis
1. Análisis de variables numéricas frente a categóricas
1. Análisis de variables categóricas frente a categóricas
1. Análisis de variables numéricas frente a numéricas
1. Truquitos para hacer gáficas monas
