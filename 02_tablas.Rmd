# Importar y manejar tablas

Aquí aprenderemos a leer tablas y  manipularlas (no, no vamos a cambiar los datoas para que nos salga lo que queremos,
ni que fuera esto un Western blot). Será divertido.

Todo esto lo vamos a hacer en el entorno tidyverse, así que primero hay que cargar los paquetes.

```{r}
library(tidyverse)
```

## Ponte en la carpeta correcta

R no es adivino, no sabe dónde tiene que buscar tus archivos. En su lugar, cuando abres R, este define un "working directory",
que como su nombre indica, es la carpeta donde está trabajando. Podemos saber cuál es:

```{r}
getwd()
```

Obviamente, podemos cambiarla:

```{r eval = FALSE}
setwd("/home/joan/cositas")
```

Sabed que si abrís RStudio haciendo doble clic en un script, el "working directory" se definirá como la carpeta donde
está ese script que habéis abierto.
Por simplicidad, os recomiendo poner los archivos que queráis leer en la misma carpeta que el script y abrir RStudio así.
Alternativamente, podéis poner en la primera línea del script la función setwd con la carpeta donde tenéis las cosas.
Si hacéis esto, las barras hay que ponerlas como / y no como barras invertidas (\\) que es lo que por algún motivo usa
Windows.

## Cargar una tabla

Como ejemplo, usaremos estos datos de Kaggle. <https://www.kaggle.com/sulianova/cardiovascular-disease-dataset>.
Me importa bastante poco qué son y de dónde han salido, la verdad (no, no debería darme igual, pero esto es un cursillo
cutre al fin y al cabo).
Lo que sí que me importa es qué es cada columna y cómo está codificada. En el link lo pone, os lo miráis.

Carguemos los datos y veamos las primeras filas:

\pagebreak

```{r}
datos = read_tsv("cardio_small.tsv")
head(datos)
```

Ya hemos cargado la tabla y la hemos guardado en la variable "datos". Obviamente, el nombre de la variable puede ser el
que nos dé la gana.

Estaría bien conocer un poco estos datos:

```{r}
nrow(datos)
ncol(datos)
```

Vemos que la tabla tiene 13 columnas (variables) y 401 filas (observaciones).
Si os habéis fijado en cuando hemos visto las primera filas con la función `head`, tenemos unos cuantos problemas que solucionar.
Pero antes...

## Recodificar, categorizar y crear nuevas variables

El panorama es bastante desolador. Si no os habéis dado cuenta de por qué, no pasa nada, aún sois principiantes.
Mirad, por ejemplo, la variable "gender". Tidyverse se ha pensado que es una variable numérica y la ha codificado como
"double" (número con decimales).
En realidad, es una variable categórica. Para este estudio solo se han cogido hombres y mujeres, así que en este caso,
el género lo consideraremos una variable categórica binaria (con dos categorías).


## Recodificar variables

El panorama es bastante desolador. Si no os habéis dado cuenta de por qué, no pasa nada, aún sois principiantes.
Mirad, por ejemplo, la variable "gender". Tidyverse se ha pensado que es una variable numérica y la ha codificado como
"double" (número con decimales).
En realidad, es una variable categórica. Para este estudio solo se han cogido hombres y mujeres, así que en este caso,
el género lo consideraremos una variable categórica binaria (con dos categorías).
En lugar de tener unos y doses en esta columna, deberíamos tener "hombre" y "mujer".

```{r}
datos = datos %>% mutate(gender = recode(gender, `1` = "mujer", `2` = "hombre"))
head(datos)
```

Las primeras filas de la tabla nos demuestran que ha funncionado. "gender" es ahora una variable del tipo "chr"
(character, es decir, palabras y no números) y donde antes había unos y doses ahora hay mujeres y hombres.

Aquí han pasado varias cosas, así que vamos a ir desgranándolas una a una:

Primero, la línea de código empieza con:

    datos = 

Eso significa que, lo que hagamos a la derecha del igual, lo vamos a **reasignar** a la variable `datos`.
Como `datos` era una tabla y lo va a seguir siendo, sabemos que lo de la derecha nos va a devolver otra vez la misma
tabla, pero con una modificación en la columna `gender`.

    datos %>%

Esto: `%>%` es lo que se llama un "pipe", tubería en inglés.
Es algo que introduce el entorno tidyverse (R de base no funciona así) y que tiene como idea poder encadenar comandos.
En este caso, implica que al comando que viene después (`mutate`) le vamos a pasar la tabla `datos` para que la use para sus cosas.

    mutate(gender = 

La función `mutate` es una función introducida por Tidyverse.
Básicamente, lo que hace es crear una nueva columna y meterla en la tabla.
En este caso, estamos creando la columna "gender". Como ya existía, lo que estamos haciendo en realidad es modificarla.
Si le pusiéramos otro nombre, por ejemplo "gender2", estaríamos manteniendo la columna original "gender", con sus unos y doses
y crearíamos una nueva, llamada "gender2", al final de la tabla. Esto no es lo que nos interesa.

    recode(gender, `1` = "hombre", `2` = "mujer")

La función `recode` también es introducida por Tidyverse.
Le hemos pasado tres argumentos: el primero es el nombre de la columna **original**.
Luego, le decimos que los "1" tienen que pasar a ser "hombre" y los "2" tienen que pasar a ser mujer.
Los números tienen que ir rodeados de acentos abiertos (\`) para que R sepa que nos estamos refiriendo al contenido de la tabla.
"hombre" y "mujer", como siempre, tienen que ir entre comillas.

    )

No hay que olvidarse de cerrar ningún paréntesis! Todo lo de `gender = recode(blablablabla)` lo estábamos haciendo como argumento
de la función `mutate`.
Ahora, ya hemos acabado de `mutate` y cerramos el paréntesis.

Esto que hemos hecho para "gender" lo podemos hacer para el resto de variables. Es más, lo podemos hacer a la vez.

```{r}
datos = datos %>%
    mutate(cholesterol = recode_factor(cholesterol, `1` = "normal", `2` = "above normal",
                                       `3` = "well above normal"),
           gluc = recode_factor(gluc, `1` = "normal", `2` = "above normal",
                                `3` = "well above normal"),
           smoke = recode_factor(smoke, `0` = "no", `1` = "si"),
           alco = recode_factor(alco, `0` = "no", `1` = "si"),
           active = recode_factor(active, `0` = "no", `1` = "si"),
           cardio = recode_factor(cardio, `0` = "sano", `1` = "enfermo")
           )
```

Los saltos de línea los podemos poner donde queramos (R no los interpreta) para que el código quede más fácil de leer.

Notaréis que aquí he usado la función `recode_factor` en lugar de `recode`.
He hecho esto porque "gender" era una variable categórica no ordinal.
A la hora de hacer cálculos, gráficas, etc. una de las categorías necesariamente irá antes de la otra, pero nos da igual cuál sea.
En el caso del colesterol y la glucosa, las categorías sí siguen un orden intrínseco, como podéis ver, así que es importante preservar el orden.
Esto lo hacemos convirtiéndolas a "factor".

En el caso de las tres últimas, yo no las consideraría variables ordinales, pero prefiero definir yo la referencia.
Por ejemplo, para el alcohol, cuando hagamos gráficos, saldrán a la izquierda los "no" y a la derecha los "sí".

## Computar nuevas variables

Podemos crear variables nuevas a partir de las originales. Por ejemplo, calculemos el IMC:

```{r}
datos = datos %>% mutate(bmi = weight/(height/100)^2)
datos = datos %>% mutate(age_years = age/365)
```

Para hacer esto, también usamos la función `mutate`.
Aquí, como queremos una variable nueva (y no cambiar una que ya existía) el nombre que le damos, "bmi", es nuevo.
Además, el cálculo de "bmi" viene a partir de otras variables ("weight" y "height").

## Categorizar una variable numérica

Podemos dividir una variable numérica en categorías:

```{r}
datos = datos %>% mutate(bmi_cat = case_when(bmi < 18.5 ~ "underweight",
                                             bmi >= 18.5 & bmi < 25 ~ "normal",
                                             bmi >= 25 ~ "overweight"
                                             )
                         )
```

Esta nueva variable que hemos creado también es ordinal, así que deberíamos definir el orden.
Como ya la hemos creado, lo podemos hacer así:

```{r}
datos$bmi_cat = fct_relevel(datos$bmi_cat, levels = c("underweight", "normal", "overweight"))
```

Es la primera vez que usamos el dólar. El dólar se utiliza en R base para referirnos a una columna de una tabla.
Aquí, lo que estamos haciendo es "recrear" la columna con los mismos datos, pero con las categorías ahora ordenadas.


## Importar tablas y saber cómo lo estamos haciendo

He alargado el momento de explicar esto lo más que he podido.

R lee archivos de texto. No lee Excels ni mierdas. Lee archivos de texto.
¿Cómo guardamos una tabla en un archivo de texto?
Pues podemos hacer que cada vez que pasemos a una columna nueva, haya una coma. O un tabuldor. O un punto y coma.
Antes, hemos leído los datos así:

    datos = read_tsv("cardio_small.tsv")

¿Por qué? Porque estaban guardados usando un tabulador como delimitador.
Si el delimitador hubiera sido una coma, podríamos haberlo hecho así:

    datos = read_tsv("cardio_train.csv")

Se pueden guardar tablas de Excel como archivos de texto. Google es su amigo.
Ustedes controlan cuál es el delimitador.
Ustedes también controlan si los decimales son una coma o un punto.

```{r echo = FALSE}
saveRDS(datos, "datos.rds")
```
